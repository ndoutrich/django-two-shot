from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receipts"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["receipts"] = Receipt.objects.filter(
    #         purchaser=self.request.user
    #     )
    #     return context

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def get_form(self, *args, **kwargs):
        form = super(ReceiptCreateView, self).get_form(*args, **kwargs)
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=self.request.user
        )
        form.fields["account"].queryset = Account.objects.filter(
            owner=self.request.user
        )
        return form

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template = "receipts/expense_categories/list.html"
    context_object_name = "categories"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template = "receipts/accounts/list.html"
    context_object_name = "accounts"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template = "receipts/expense_categories/new.html"
    fields = ["name"]
    success_url = reverse_lazy("list_categories")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template = "receipts/accounts/new.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("list_accounts")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)
